Live Demo: [https://icewassim.gitlab.io/trip-search](https://icewassim.gitlab.io/trip-search).

## Available Scripts

In the project directory, you can run:
### `yarn` to install the Project

### `yarn start` to start the Project

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test` , (requires a paralell runing instance of the app on port :3000)

Launches the test runner with cypress, it requires that you have a running app on port 3000
