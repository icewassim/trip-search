import {combineReducers} from 'redux';

import {ACTION_TYPE} from '../constants';


const tripsReducer = (trips = [], action) => {
  if (action.type === ACTION_TYPE.FETCH_TRIPS) {
    return action.payload;
  }

  return trips;
};

const isLoadingTripsReducer = (isLoading = false, action) => {
  if (action.type === ACTION_TYPE.LOADING_TRIPS) {
    return true;
  }

  if (action.type === ACTION_TYPE.FETCH_TRIPS) {
    return false;
  }

  return isLoading;
}

const reducers = combineReducers({
    trips: tripsReducer,
    isLoadingTrips: isLoadingTripsReducer,
});

export default reducers;
