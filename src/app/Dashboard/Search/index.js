import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { AutoComplete, Row, Col, Icon, Button } from 'antd';
import './Search.css';

import staticCityNames from './staticCityNames';

const Search = ({onSearch}) => {
    const [departure, setDeparture] = useState('');
    const [destination, setDestination] = useState('');
    const handleClick = () => {
        onSearch(departure, destination);
    }

    return (<Row>
        <Col span={20} offset={2}>
            <AutoComplete
                value={departure}
                dataSource={staticCityNames}
                placeholder="Departure"
                className="search-bar"
                onSelect={setDeparture}
                onChange={setDeparture}
                size="large"
                id="departure-input"
                enterButton
            />
            <Icon
                type="arrow-right"
                className="arrow-direction"
            />
            <AutoComplete
                dataSource={staticCityNames}
                placeholder="Destination"
                className="search-bar"
                value={destination}
                onSelect={setDestination}
                onChange={setDestination}
                size="large"
                id="destination-input"
                enterButton
            />

            <Button
                type="primary"
                shape="circle"
                icon="search"
                className="search-btn"
                id="search-button"
                size="large"
                onClick={handleClick}
            />
        </Col>
    </Row>
)}

Search.propTypes = {
    onSearch: PropTypes.func.isRequired,
};

export default Search;