import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import Search from './Search';
import TripList  from './Trips';

const Dashboard = ({trips, fetchTrips, isLoadingTrips}) => {
    useEffect(() => {
        fetchTrips();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (<>
            <Search onSearch={fetchTrips}/>
            <TripList 
                trips={trips}
                isLoadingTrips={isLoadingTrips}
            />
        </>)
}

Dashboard.propTypes = {
    trips: PropTypes.array,
    isLoadingTrips: PropTypes.bool.isRequired,
    fetchTrips: PropTypes.func.isRequired,
};

export default Dashboard;