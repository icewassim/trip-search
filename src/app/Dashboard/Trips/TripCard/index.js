import React from 'react';
import PropTypes from 'prop-types';
import { Card, Avatar, Tag, Steps, Icon } from 'antd';

import './TripCard.css';


const { Meta } = Card;
const { Step } = Steps;

const TripCard = ({id, arrival, departure, price, idx , user}) => (
        <Card className="trip-card" data-cy={`card-result-${idx}`}>
            <div className="price-tag" data-cy={`price-${idx}`} >
                <Tag color={price.color} size="large">{price.value}</Tag>
            </div>
            <Meta
                avatar={
                    <Avatar src={user.picture} size="large" />
                }
                title={`${departure.city} - ${arrival.city}`}
                description={user.name}
            />
            <br />
            <div className="departure-date-container">
                <p>
                    <Icon type="clock-circle" style={{ marginRight: '5px' }} />
                    <span data-cy={`date-${idx}`} >{departure.date}</span>
                </p>
            </div>
            <Steps progressDot current={-1}>
                <Step title={departure.address} data-cy={`departure-${idx}`} />
                <Step title={arrival.address} data-cy={`arrival-${idx}`} />
            </Steps>
        </Card>
)

TripCard.propTypes = {
    trips: PropTypes.arrayOf(PropTypes.object),
    id: PropTypes.string,
    arrival: PropTypes.object,
    departure: PropTypes.object,
    price: PropTypes.object,
    idx: PropTypes.number,
    user: PropTypes.object,
};

export default TripCard;