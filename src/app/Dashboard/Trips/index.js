import React from 'react';
import PropTypes from 'prop-types';

import { Row, Col, Empty } from 'antd';

import './Trips.css';
import LoadingSkeleton from './LoadingSkeleton';
import TripCard from './TripCard';

const Trips = ({ trips, isLoadingTrips }) => {
    if (isLoadingTrips) {
        return <LoadingSkeleton />
    }

    if(!trips.length) {
        return <Empty data-cy={`no-trips`} className="empty-result"/>
    }

    return (
        <Row>
            <Col span={16} offset={8}>
                {
                    trips.map(({ user, id, departure, arrival, price }, idx) => (
                        <TripCard 
                            key={id}
                            id={id}
                            user={user}
                            departure={departure}
                            arrival={arrival}
                            price={price}
                            idx={idx}
                        />
                    ))
                }
            </Col>
        </Row>
    )
};

Trips.propTypes = {
    trips: PropTypes.arrayOf(PropTypes.object).isRequired,
    isLoadingTrips: PropTypes.bool.isRequired,
};

export default Trips;