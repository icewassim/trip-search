import { message } from 'antd';

import config from '../../../config';
// replace it with @app
import blablacar from '../../../apis/blablacar';
import { ACTION_TYPE } from '../../../constants';

export const fetchTripsActionCreator = (departure = 'Paris' , destination = 'Rennes') => async dispatch => {
    try {
        dispatch({ type: ACTION_TYPE.LOADING_TRIPS });

        const queryStringParms = `${config.tripsPath}&fn=${departure}&tn=${destination}`;
        const { data } = await blablacar.get(queryStringParms);
        dispatch({
            type: ACTION_TYPE.FETCH_TRIPS,
            payload: data.trips,
        });
    } catch (err) {
        message.error('Something went wrong could not fetch trips');
    }
};