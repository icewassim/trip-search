export const getTrips = (trips) => trips && trips.map(({
    permanent_id,
    price,
    user,
    departure_place,
    arrival_place,
    departure_date,
}) => ({
    id: permanent_id,
    user: {
        name: user && user.display_name,
        picture: user && user.picture,
    },
    price: {
        value: price && price.string_value,
    },
    departure: {
        date: departure_date,
        address: departure_place && departure_place.address,
        city: departure_place && departure_place.city_name,
    },
    arrival: {
        address: arrival_place && arrival_place.address,
        city: arrival_place && arrival_place.city_name,
    }
}));