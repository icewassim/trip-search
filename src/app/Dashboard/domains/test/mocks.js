export const mockAPIData = [{
    permanent_id: 'some-random-id',
    departure_date: '01/01/2019 08:00',
    price: {
        string_value: '1234 Euro',
        color: 'RED',
    },
    user: {
        display_name: 'TestUser',
        picture: 'http://dsqdsq',
    },
    departure_place: {
        address: 'dep adress',
        city_name: 'dep city',
    },
    arrival_place: {
        address: 'arr adress',
        city_name: 'arr city',
    }
},
{
    permanent_id: 'some-random-id',
    departure_date: '01/01/2019 08:00',
    price: {
        string_value: '1234 Euro',
        color: 'RED',
    },
    user: {
        display_name: 'TestUser',
        picture: 'http://dsqdsq',
    },
    departure_place: {
        address: 'dep adress',
        city_name: 'dep city',
    },
    arrival_place: {
        address: 'arr adress',
        city_name: 'arr city',
    }
}];

export const expectedResult = [{
    id: 'some-random-id',
    departure: {
        date: '01/01/2019 08:00',
        address: 'dep adress',
        city: 'dep city',
    },
    arrival: {
        address: 'arr adress',
        city: 'arr city',
    },
    price: {
        value: '1234 Euro',
    },
    user: {
        name: 'TestUser',
        picture: 'http://dsqdsq',
    }
},
    {
        id: 'some-random-id',
        departure: {
            date: '01/01/2019 08:00',
            address: 'dep adress',
            city: 'dep city',
        },
        arrival: {
            address: 'arr adress',
            city: 'arr city',
        },
        price: {
            value: '1234 Euro',
        },
        user: {
            name: 'TestUser',
            picture: 'http://dsqdsq',
        }
    }];

export const mockAPIDataMissingAttributes = [{
    permanent_id: 'some-random-id',
    departure_date: '01/01/2019 08:00',
    price: {
        string_value: '1234 Euro',
        color: 'RED',
    },
    user: {
        picture: 'http://dsqdsq',
    },
    departure_place: {
        city_name: 'dep city',
    },
    arrival_place: {
        address: 'arr adress',
        city_name: 'arr city',
    }
}];

export const expectedResultMissingSomeAttributes = [{
    id: 'some-random-id',
    departure: {
        date: '01/01/2019 08:00',
        address: undefined,
        city: 'dep city',
    },
    arrival: {
        address: 'arr adress',
        city: 'arr city',
    },
    price: {
        value: '1234 Euro',
    },
    user: {
        name: undefined,
        picture: 'http://dsqdsq',
    }
}];
