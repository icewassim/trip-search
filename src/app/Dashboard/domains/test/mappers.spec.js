import { getTrips } from '../mappers';

import {
    mockAPIData,
    expectedResult,
    mockAPIDataMissingAttributes,
    expectedResultMissingSomeAttributes,
} from './mocks';

describe('Trips mapper', () => {
    it('should map correctly the trips value from the api', () => {
        const result = getTrips(mockAPIData);
        expect(result).toStrictEqual(expectedResult);
    });

    it('should return an empty array if no data', () => {
        const result = getTrips([]);
        expect(result).toStrictEqual([]);
    });

    it('should not return an exception of some attribute is missing in the api', () => {
        const result = getTrips(mockAPIDataMissingAttributes);
        expect(result).toStrictEqual(expectedResultMissingSomeAttributes);
    });
})