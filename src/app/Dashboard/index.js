import { connect } from 'react-redux';

import { getTrips } from './domains/mappers';

import {
    fetchTripsActionCreator,
} from './domains/actions';

import Dashboard from './container';

const mapStateToProps = ({ trips, isLoadingTrips }) => {
    return {
        trips: getTrips(trips),
        isLoadingTrips,
    };
};

export default connect(mapStateToProps, {
    fetchTrips: fetchTripsActionCreator,
})(Dashboard);