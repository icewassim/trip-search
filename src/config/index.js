export default {
    baseUrl: process.env.REACT_APP_BASE_URL || 'https://edge.blablacar.com/api/v2/',
    refreshTokenUrl: process.env.REACT_APP_REFRESH_TOKEN_URL || 'https://edge.blablacar.com/token',
    tripsPath: process.env.REACT_APP_TRIP_BASE_URL || 'trips?_format=json&locale=fr_FR&cur=EUR'
}