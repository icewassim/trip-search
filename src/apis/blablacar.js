import axios from 'axios';
import config from '../config';

class BlablaAPI {
  constructor(clientCredentials) {
    this.tokens = JSON.parse(localStorage.getItem('tokens')) || {};
    this.clientCredentials = clientCredentials || {
      client_id: 'dummy-client',
      client_secret: 'UizxdvqmPxhO7s3IPqy9C5bkmTU9B0zL',
    };
  }

  updateAccessToken(accessTokenData) {
    const tokens = {
      ...this.tokens,
      ...accessTokenData,
    };

    window.localStorage.setItem('tokens', JSON.stringify(tokens));
    this.tokens = tokens;
  }

  tokenHasExpired() {
    const { issued_at, expires_in } = this.tokens;
    const currentTime = (new Date().getTime() / 1000);
    return (issued_at + expires_in) < currentTime;
  }

  async refreshTokenIfNeeded() {
    if (this.tokens.access_token &&!this.tokenHasExpired()) {
      return Promise.resolve();
    }
    console.log('token has expired', this.tokenHasExpired());

    try {
      const { data: {
        expires_in,
        issued_at,
        token_type,
        access_token
      } } = await axios.post(config.refreshTokenUrl, {
        grant_type: 'client_credentials',
        ...this.clientCredentials,
        scopes: [
          'SCOPE_TRIP_DRIVER',
          'DEFAULT',
          'SCOPE_INTERNAL_CLIENT'
        ]
      });

      const refreshedToken = {
        expires_in,
        issued_at,
        token_type,
        access_token,
      };

      this.updateAccessToken(refreshedToken);
      return refreshedToken;
    } catch (err) {
      console.error('could not refresh token, session expired, invalid clientID. logout user', err);
    }
  }

  async post(url, options) {
    await this.refreshTokenIfNeeded();
    return axios.post(url, options, {
      baseURL: config.baseUrl,
      headers: { 'Authorization': `Bearer ${this.tokens.access_token}` }
    });
  }

  async get(url) {
    await this.refreshTokenIfNeeded();
    return axios.get(url, {
      baseURL: config.baseUrl,
      headers: { 'Authorization': `Bearer ${this.tokens.access_token}` }
    });
  }

  updateClientCredentials(clientCredentials) {
    this.clientCredentials = clientCredentials;
  }
}

export default new BlablaAPI();