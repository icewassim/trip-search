import config from '../../config';
import axios from 'axios';
import blablacar from '../blablacar';

var localStorageMock = (function () {
    var store = {};

    return {
        getItem: function (key) {
            return store[key] || null;
        },
        setItem: function (key, value) {
            store[key] = value.toString();
        },
        clear: function () {
            store = {};
        }
    };

})();

Object.defineProperty(window, 'localStorage', {
    value: localStorageMock
});

const mockToken = {
    expires_in: 3600,
    issued_at: 'hello',
    token_type: 'hello',
    access_token: 'toBeStoredAccessToken',
}

const mockClientCrentials = {
    "client_id": "dummy-client",
    "client_secret": "UizxdvqmPxhO7s3IPqy9C5bkmTU9B0zL",
    "grant_type": "client_credentials",
    "scopes": ["SCOPE_TRIP_DRIVER", "DEFAULT", "SCOPE_INTERNAL_CLIENT"]
}

describe('blablacarPI ', () => {
    describe('refresh token if needed', () => {
        it('should get a new token if there is none', async () => {
            axios.post.mockResolvedValue({
                data: {
                    ...mockToken,
                    access_token: 'newly_refreshed_token2',
                }
            });

            blablacar.updateAccessToken(null);
            const updateAccessTokenSpy = jest.spyOn(blablacar, 'updateAccessToken');
            const refreshedToken = await blablacar.refreshTokenIfNeeded();
            expect(axios.post).toBeCalledWith(config.refreshTokenUrl, mockClientCrentials);
            expect(updateAccessTokenSpy).toBeCalledWith({
                ...mockToken,
                access_token: 'newly_refreshed_token2',
            });
            blablacar.updateAccessToken.mockClear();
            expect(refreshedToken).toStrictEqual({
                ...mockToken,
                access_token: 'newly_refreshed_token2',
            });
        });

        it('should refreshToken When it has expired', async () => {
            axios.post.mockResolvedValue({
                data: {
                    ...mockToken,
                    access_token: 'newly_refreshed_token',
                }
            });

            blablacar.updateAccessToken({
                ...mockToken,
                issued_at: 1573591924 // 12/11/2019
            });
            const updateAccessTokenSpy = jest.spyOn(blablacar, 'updateAccessToken');
            const refreshedToken = await blablacar.refreshTokenIfNeeded();
            expect(axios.post).toBeCalledWith(config.refreshTokenUrl, mockClientCrentials);
            expect(updateAccessTokenSpy).toBeCalledWith({
                ...mockToken,
                access_token: 'newly_refreshed_token',
            });

            blablacar.updateAccessToken.mockClear();
            expect(refreshedToken).toStrictEqual({
                ...mockToken,
                access_token: 'newly_refreshed_token',
            });
        });

        it('should not refresh token if not needed', async () => {
            blablacar.updateAccessToken({
                ...mockToken,
                issued_at: 2573591924 // 21/03/2051
            });

            axios.post = jest.fn();
            blablacar.updateAccessToken.mockClear();
            const updateAccessTokenSpy = jest.spyOn(blablacar, 'updateAccessToken');
            await blablacar.refreshTokenIfNeeded();
            expect(axios.post).not.toHaveBeenCalled()
            expect(updateAccessTokenSpy).not.toHaveBeenCalled();
        });
    });

    describe('token utils', () => {
        beforeEach(() => {
            jest.clearAllMocks();
        });

        it('blablacar.updateAccessToken shoudl update token', async () => {
            blablacar.updateAccessToken(mockToken);
            expect(JSON.parse(localStorageMock.getItem('tokens')))
                .toStrictEqual(mockToken);
            await blablacar.get('trips');
            expect(axios.get).toBeCalledWith('trips', {
                baseURL: config.baseUrl,
                headers: {
                    Authorization: 'Bearer toBeStoredAccessToken'
                }
            });
        });

        it('blablacar.tokenHasExpired should check token expiration', async () => {
            blablacar.updateAccessToken({
                ...mockToken,
                issued_at: 1573591924 // 12/11/2019
            });
            expect(blablacar.tokenHasExpired()).toBe(true);

            blablacar.updateAccessToken({
                ...mockToken,
                issued_at: 2573591924 // 21/03/2051
            });
            expect(blablacar.tokenHasExpired()).toBe(false);
        });
    });

    describe('post/get utils', () => {
        beforeEach(() => {
            jest.clearAllMocks();
        });

        it('make a get call with headers', async () => {
            blablacar.updateAccessToken(mockToken);
            blablacar.refreshTokenIfNeeded = jest.fn();
            await blablacar.get('hellothere');
            expect(axios.get).toBeCalledWith('hellothere', {
                baseURL: config.baseUrl,
                headers: {
                    Authorization: 'Bearer toBeStoredAccessToken'
                }
            });
            expect(blablacar.refreshTokenIfNeeded).toHaveBeenCalled()
        });

        it('make a post call with headers', async () => {
            blablacar.updateAccessToken(mockToken);
            blablacar.refreshTokenIfNeeded = jest.fn();
            await blablacar.post('hellothere', {
                mockOptions: 'hello'
            });
            expect(axios.post).toBeCalledWith('hellothere',{
                mockOptions: 'hello',
            },{
                baseURL: config.baseUrl,
                headers: {
                    Authorization: 'Bearer toBeStoredAccessToken'
                }
            });
            expect(blablacar.refreshTokenIfNeeded).toHaveBeenCalled()
        });
    });
})