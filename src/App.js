import React from 'react';

import './App.css';
import Dashboard from './app/Dashboard';

const App = () => (
  <div className="App">
    <Dashboard />
  </div>
);

export default App;
