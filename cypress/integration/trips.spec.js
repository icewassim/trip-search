/// <reference types="Cypress" />

import mockResponse from '../support/mocks/mock_response';
import mockResponseLyon from '../support/mocks/mock_response_paris_lyon';
import mockEmptyList from '../support/mocks/mock_no_result';

context('Display trips && trip info', () => {
  beforeEach(() => {
    cy.server()
    cy.route({
      method: 'GET',
      url: 'https://edge.blablacar.com/api/v2/trips*',
      response: mockResponse,
    });

    cy.visit('http://localhost:3000');
  });

  it('should Display drivers name', () => {
    cy.get('[data-cy=card-result-0]')
      .should('be.visible')

    cy.get('.trip-card').should('have.length', 10);
  });

  it('should Display departure and arrival', () => {
    cy.get('[data-cy=card-result-1]')
      .should('be.visible')
      .find('.ant-card-meta-title')
      .first()
      .should('be.visible')
      .should('contain', 'Villeneuve-Saint-Georges - Rennes')

    cy.get('[data-cy=departure-0]')
      .should('be.visible')
      .should('contain', 'Longjumeau, France')

    cy.get('[data-cy=arrival-0]')
      .should('be.visible')
      .should('contain', 'Brécé, France')
  });

  it('should Display date', () => {
    cy.get('[data-cy=date-0]')
      .should('be.visible')
      .should('contain', '17/11/2019 20:00:00')
  });
});


context('Search by Departure and destination', () => {
  beforeEach(() => {
    cy.server()
    cy.route({
      method: 'GET',
      url: 'https://edge.blablacar.com/api/v2/trips?_format=json&locale=fr_FR&cur=EUR&fn=Paris&tn=Rennes',    // that have a URL that matches '/users/*'
      response: mockResponse       // and force the response to be: []
    })

    cy.route({
      method: 'GET',
      url: 'https://edge.blablacar.com/api/v2/trips?_format=json&locale=fr_FR&cur=EUR&fn=Paris&tn=Lyon',    // that have a URL that matches '/users/*'
      response: mockResponseLyon       // and force the response to be: []
    })

    cy.visit('http://localhost:3000');
  })

  it('should Display departure and arrival', () => {
    cy.get('#departure-input')
      .should('be.visible')
      .type('Paris')
      .get('#destination-input')
      .should('be.visible')
      .type('Lyon')
      .get('#search-button')
      .should('be.visible')
      .click()

    cy.get('[data-cy=date-0]')
      .should('be.visible')
      .should('contain', '17/11/2019 21:00:00')
  });
})

context('Handle empty result list', () => {
  beforeEach(() => {
    cy.server()
    cy.route({
      method: 'GET',
      url: 'https://edge.blablacar.com/api/v2/trips*',
      response: mockEmptyList,
    });

    cy.visit('http://localhost:3000');
  });

  it('should Display departure and arrival', () => {
    cy.get('[data-cy=no-trips]')
      .should('be.visible');
  });
});